# Copyright 2011-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=user-none ] cmake [ api=2 cmake_minimum_version=3.0 ]
require python [ min_versions="3.4" blacklist="2" multibuild=false ]

export_exlib_phases src_prepare

SUMMARY="Sigil is a multi-platform F/OSS WYSIWYG epub ebook editor"
DESCRIPTION="
Uses Qt5.
Full Unicode support: everything you see in Sigil is in UTF-16
Full EPUB spec support
Multiple Views: Book View, Code View and Split View
Metadata editor with full support for all possible metadata entries (more than 200) with full descriptions for each
Table Of Contents editor
Multi-level TOC support
Book View fully supports the display of any XHTML document possible under the OPS spec
SVG support
Basic XPGT support
Advanced automatic conversion of all imported documents to Unicode
Currently imports TXT, HTML and EPUB files; more will be added with time
"
HOMEPAGE="http://sigil-ebook.com"
BUGS_TO="philantrop@exherbo.org"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-spell/hunspell:=
        dev-libs/boost
        dev-libs/pcre
        dev-python/lxml[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        x11-libs/qtbase:5[>=5.4.0]
        x11-libs/qtmultimedia:5[>=5.4.0]
        x11-libs/qtsvg:5[>=5.4.0]
        x11-libs/qttools:5[>=5.4.0]
        x11-libs/qtwebkit:5[>=5.4.0]
"

Sigil_src_prepare() {
    cmake_src_prepare

    edo sed -e "/find_package(PythonInterp/ s/3.4/$(python_get_abi)/" \
            -e "/find_package (PythonLibs/ s/3.4/$(python_get_abi)/" \
            -i CMakeLists.txt
}

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DSHARE_INSTALL_PREFIX=/usr
    -DUSE_SYSTEM_LIBS=true
)


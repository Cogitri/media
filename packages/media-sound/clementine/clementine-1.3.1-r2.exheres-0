# Copyright 2010 Timothy Redaelli
# Copyright 2011 Fabian Holler
# Copyright 2011 Bo Ørsted Andresen
# Copyright 2012 Lasse Brun <bruners@gmail.com>
# Copyright 2016 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

## TODO: Using builtin sha2 library, which seems unmaintained upstream?
##       http://www.aarongifford.com/computers/sha.html
## TODO: Internal copy of libmygpo-qt is used, since upstream hasn't released 1.0.9 yet, we can't
##       fix this yet.
## TODO: Has an internal copy of sqlite3, which isn't built?

require freedesktop-desktop gtk-icon-cache github [ user=clementine-player pn=Clementine ] cmake [ api=2 ]

SUMMARY="A modern music player and library organizer based on Amarok 1.4 and Qt4"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
HOMEPAGE="http://www.clementine-player.org/ ${HOMEPAGE}"

MYOPTIONS="
    cd              [[ description = [ Audio CD support ] ]]
    ipod            [[ description = [ iPod classic, iPod Touch, iPhone, and iPad support ] ]]
    lastfm          [[ description = [ Fetch song info, scrobbling and radio streams with last.fm ] ]]
    moodbar         [[ description = [ Builds the bundled gst-plugins-moodbar for moodbar support ] ]]
    mtp             [[ description = [ support for MTP devices ] ]]
    pulseaudio
    visualisations  [[ description = [ libprojectm visualisations ] ]]
    wiimote         [[ description = [ support for WII-Remote ] ]]
"

DEPENDENCIES="
    build:
        dev-libs/boost
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/boost
        dev-libs/crypto++[>=5.6.3] [[ note = [ Our first version to provide a .pc file ] ]]
        dev-libs/glib:2
        dev-libs/libxml2:2.0
        dev-libs/protobuf:=
        dev-libs/qjson
        media-libs/chromaprint
        media-libs/glew[>=1.5.0]
        media-libs/gstreamer:1.0
        media-libs/libechonest[qt4(+)]
        media-libs/libspotify[>=12.1.45]
        media-libs/taglib[>=1.8]
        media-plugins/gst-plugins-base:1.0
        x11-libs/qt:4[>=4.5.0][X(+)][dbus][opengl][sqlite]
        cd? ( dev-libs/libcdio )
        ipod? (
            app-pda/libimobiledevice[>=1.0]
            app-pda/usbmuxd
            dev-libs/libplist
            media-libs/libgpod[>=0.7.92]
        )
        lastfm? ( media-libs/liblastfm[qt4(+)] )
        moodbar? ( sci-libs/fftw[>=3.0.0] )
        mtp? ( media-libs/libmtp[>=1.0] )
        pulseaudio? ( media-sound/pulseaudio )
    run:
        media-plugins/gst-plugins-good:1.0[gstreamer_plugins:taglib]
    suggestion:
        media-plugins/gst-plugins-ugly:1.0[gstreamer_plugins:lame] [[ description = [ encode mp3s with gstreamer ] ]]
        media-plugins/gst-plugins-ugly:1.0[gstreamer_plugins:mad]  [[ description = [ decode mp3s with gstreamer ] ]]
"

UPSTREAM_RELEASE_NOTES="http://www.clementine-player.org/#news"
UPSTREAM_CHANGELOG="https://github.com/clementine-player/Clementine/blob/master/Changelog"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Fix-crash-with-sqlite-3.11.x.patch
    "${FILES}"/0002-Fix-caps-on-audio-pipeline.patch
    "${FILES}"/0003-Add-compatibility-with-chromaprint-1.4.patch
    "${FILES}"/0004-Fix-compilation-with-GCC-6.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_WERROR=OFF
    -DENABLE_AMAZON_CLOUD_DRIVE:BOOL=OFF
    -DENABLE_BOX:BOOL=OFF
    -DENABLE_DBUS:BOOL=ON
    -DENABLE_DEVICEKIT:BOOL=OFF
    -DENABLE_DROPBOX:BOOL=OFF
    -DENABLE_GIO:BOOL=ON
    -DENABLE_GOOGLE_DRIVE:BOOL=OFF
    -DENABLE_SEAFILE:BOOL=OFF
    -DENABLE_SKYDRIVE:BOOL=OFF
    -DENABLE_SPOTIFY_BLOB:BOOL=OFF
    -DENABLE_VK:BOOL=OFF
    -DUSE_BUILTIN_TAGLIB:BOOL=OFF
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'cd AUDIOCD'
    'ipod LIBGPOD'
    'lastfm LIBLASTFM'
    MOODBAR
    'mtp LIBMTP'
    'pulseaudio LIBPULSE'
    VISUALISATIONS # libprojectm is optionally bundled with this option enabled
    'wiimote WIIMOTEDEV'
)

src_install() {
    cmake_src_install
    # .desktop files belong in /usr/share/ instead of /usr/host/share/
    edo mv "${IMAGE}"/usr/{$(exhost --target)/share/*,share}
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share
}


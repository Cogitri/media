# Copyright 2009 Daniel Mierswa <impulze@impulze.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge

SUMMARY="A console based decoder/player for mono/stereo mpeg audio files"
HOMEPAGE="https://www.${PN}.de"

REMOTE_IDS+=" freecode:${PN}"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/api [[ lang = en description = [ API reference ] ]]"

LICENCES="( GPL-2 LGPL-2.1 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    alsa
    debug
    jack
    openal
    pulseaudio
    sdl
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        alsa? ( sys-sound/alsa-lib )
        jack? ( media-sound/jack-audio-connection-kit )
        openal? ( media-libs/openal )
        pulseaudio? ( media-sound/pulseaudio )
        sdl? ( media-libs/SDL:0 )
"

src_configure() {
    local modules=()

    option alsa && modules+=( alsa )
    option jack && modules+=( jack )
    option openal && modules+=( openal )
    option pulseaudio && modules+=( pulse )
    option sdl && modules+=( sdl )

    if [ ${#modules[@]} -eq 0 ]; then
        modules+=( dummy )
    fi

    econf \
        --enable-equalizer \
        --enable-ipv6 \
        --disable-yasm \
        --with-audio="$(echo ${modules[@]} | tr ' ' ',')" \
        $(option_enable debug)
}


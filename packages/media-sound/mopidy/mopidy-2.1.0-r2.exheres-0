# Copyright 2016-2017 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ] \
    setup-py [ import=setuptools blacklist=3 has_bin=true multibuild=false test=pytest ] \
    systemd-service [ systemd_files=[ extra/systemd/mopidy.service ] ] \
    freedesktop-desktop

SUMMARY="An extensible music server with MPD and Spotify support written in Python"

REMOTE_IDS+=" pypi:Mopidy"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# broken, last checked: 2.1.0
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-python/Sphinx[python_abis:*(-)?] [[ note = [ man-pages generation ] ]]
    build+run:
        group/${PN}
        user/${PN}
        dev-python/Pykka[>=1.1][python_abis:*(-)?]
        dev-python/requests[>=2.0][python_abis:*(-)?]
        dev-python/tornado[>=3.2][python_abis:*(-)?]
        media-libs/gstreamer:1.0[>=1.2.3][gobject-introspection]
        media-plugins/gst-plugins-base:1.0[>=1.2.3][gobject-introspection][gstreamer_plugins:ogg]
        media-plugins/gst-plugins-good:1.0[>=1.2.3][gstreamer_plugins:flac][gstreamer_plugins:soup]
        media-plugins/gst-plugins-ugly[>=1.2.3][gstreamer_plugins:mpg123]
    run:
        gnome-bindings/pygobject:3[python_abis:*(-)?]
    test:
        dev-python/mock[python_abis:*(-)?]
        dev-python/pytest-capturelog[python_abis:*(-)?]
        dev-python/responses[python_abis:*(-)?]
    suggestion:
        media-plugins/gst-plugins-base:1.0[gstreamer_plugins:alsa] [[
            description = [ ALSA backend support ]
        ]]
        media-plugins/gst-plugins-good:1.0[gstreamer_plugins:pulseaudio] [[
            description = [ PulseAudio backend support ]
        ]]
        media-plugins/mopidy-iris [[
            description = [ Mopidy extension providing a Spotify-like webinterface ]
        ]]
        media-plugins/mopidy-local-images [[
            description = [ Mopidy extension for handling embedded album art ]
        ]]
        media-plugins/mopidy-local-sqlite [[
            description = [ Mopidy extension for SQLite based local library ]
        ]]
        media-plugins/mopidy-musicbox-webclient [[
            description = [ Mopidy extension providing the MusicBox webinterace ]
        ]]
        media-plugins/mopidy-scrobbler [[
            description = [ Mopidy extension for scrobbling played tracks to Last.fm ]
        ]]
        media-plugins/mopidy-spotify [[
            description = [ Mopidy extension for playing music from Spotify ]
        ]]
        net-apps/snapcast [[
            description = [ Allows to turn Mopidy into a multi-room audio solution ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-2.1.0-idle.patch
)

src_compile() {
    setup-py_src_compile

    emake -C docs man
}

src_install() {
    setup-py_src_install

    dobin extra/mopidyctl/mopidyctl

    doman docs/_build/man/mopidy.1
    doman extra/mopidyctl/mopidyctl.8

    insinto /etc/${PN}
    doins "${FILES}"/{logging,mopidy}.conf

    install_systemd_files

    insinto /usr/share/applications
    doins extra/desktop/${PN}.desktop

    keepdir /var/{cache,lib,log}/mopidy
    keepdir /var/lib/mopidy/{local,media,playlists}
    edo chown -R mopidy:mopidy "${IMAGE}"/var/{cache,lib,log}/mopidy
}


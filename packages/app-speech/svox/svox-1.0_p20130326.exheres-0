# Copyright 2016 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV=1.0+git${PV##*_p}
DEBIAN_PATCHLEVEL=5

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="SVOX Pico TTS engine - small footprint text-to-speech synthesizer"
DESCRIPTION="
The SVOX Pico engine is a software speech synthesizer for German, English (GB and US), Spanish, French and Italian.
SVOX produces a clear and distinct speech output made possible by the use of Hidden Markov Model (HMM) algorithms.
"
HOMEPAGE="https://android.googlesource.com/platform/external/${PN}"
DOWNLOADS="
    mirror://debian/pool/non-free/s/svox/${PN}_${MY_PV}.orig.tar.gz
    mirror://debian/pool/non-free/s/svox/${PN}_${MY_PV}-${DEBIAN_PATCHLEVEL}.debian.tar.xz
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-libs/popt
"

WORK=${WORKBASE}/${PN}-${MY_PV}/pico

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
)

src_prepare(){
    edo pushd "${WORKBASE}"/${PN}-${MY_PV}
        expatch "${WORKBASE}"/debian/patches/git-0-Use-long-to-store-pointer
        expatch "${WORKBASE}"/debian/patches/git-1-fix-memset-overflow-of-wcep_pI
        expatch "${WORKBASE}"/debian/patches/git-fix
        expatch "${WORKBASE}"/debian/patches
        expatch "${WORKBASE}"/debian/patches/volume
        expatch "${WORKBASE}"/debian/patches/fix-undefined
    edo popd

    autotools_src_prepare
}


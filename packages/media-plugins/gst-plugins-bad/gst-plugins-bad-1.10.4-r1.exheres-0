# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2009, 2011, 2013 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gsettings
require option-renames [ renames=[ 'gstreamer_plugins:x265 gstreamer_plugins:hevc' ] ]

SUMMARY="A set of plugins that need more quality"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="LGPL-2"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug
    gobject-introspection
    gtk-doc
    X
    gstreamer_plugins:
        apex         [[ description = [ AirPort Express Wireless Support ] ]]
        ass          [[ description = [ ASS/SSA (Advanced Substation Alpha/Substation Alpha) subtitle rendering using libass ] ]]
        bluez        [[ description = [ Support streaming media over Bluetooth using bluez ] ]]
        bs2b         [[ description = [ Improve headphone listening of stereo audio using the bs2b library ] ]]
        chromaprint  [[ description = [ Audio fingerprinting using the Chromaprint library ] ]]
        curl         [[ description = [ Support uploading data to a server using curl ] ]]
        dash         [[ description = [ Support Dynamic Adaptive Streaming over HTTP (DASH) ] ]]
        dc1394       [[ description = [ Support for getting data from IIDC FireWire cameras using libdc1394 ] ]]
        dtls         [[ description = [ Datagram Transport Layer Security (DTLS) decoder and encoder plugins ] ]]
        dts          [[ description = [ DTS (multichannel digital surround sound) audio decoding using libdca ] ]]
        eglgles      [[ description = [ EGL/GLES video sink ] ]]
        faac         [[ description = [ AAC audio encoding using faac ] ]]
        faad         [[ description = [ MPEG-2/4 AAC audio decoding using faad2 ] ]]
        fdk-aac      [[ description = [ Support for AAC encoding using the Frauenhofer AAC Codec Library ] ]]
        gtk3         [[ description = [ Gtk+3 video sink ]
                        requires =    [ gstreamer_plugins: opengl ]
                     ]]
        gsm          [[ description = [ GSM audio decoding and encoding ] ]]
        hdr          [[ description = [ OpenEXR plugin for decoding high-dynamic-range EXR images ] ]]
        hevc         [[ description = [ Enable H.265/HEVC encoding using x265 ] ]]
        jpeg2000     [[ description = [ JPEG 2000 image compression and decompression using OpenJPEG ] ]]
        kate         [[ description = [ Kate (karaoke and text codec) stream encoding/decoding using libkate and rendering using libtiger ] ]]
        ladspa       [[ description = [ Linux Audio Developer Simple Plugin API support ] ]]
        libmms       [[ description = [ Support for mms:// and mmsh:// network streams ] ]]
        lv2          [[ description = [ Support for LV2 (simple but extensible successor of LADSPA) plugins ] ]]
        mjpeg        [[ description = [ MPEG-1/2 video encoding and MPEG/DVD/SVCD/VCD video/audio multiplexing using mjpegtools ] ]]
        musepack     [[ description = [ Musepack audio decoding using libmpcdec ] ]]
        neon         [[ description = [ HTTP source handling using neon ] ]]
        ofa          [[ description = [ MusicIP audio fingerprint calculation using libofa ] ]]
        openal       [[ description = [ OpenAL sink ] ]]
        opencv       [[ description = [ Computer Vision techniques from OpenCV ] ]]
        opengl       [[
            description = [ OpenGL video sink ]
            requires = [ X ]
        ]]
        openh264     [[ description = [ H.264 encoder/decoder plugin for GStreamer ] ]]
        rtmp         [[ description = [ Support for rtmp:// and rtmpe:// network streams ] ]]
        resin        [[ description = [ DVD playback plugin ] ]]
        schroedinger [[ description = [ Dirac video encoding using schroedinger ] ]]
        sdl          [[ description = [ SDL-based audio and video output ] ]]
        sndfile      [[ description = [ Plugin to write and read various audio formats using libsndfile ] ]]
        soundtouch   [[ description = [ BPM detection and audio pitch controlling using soundtouch ] ]]
        svg          [[ description = [ SVG image decoding using librsvg ] ]]
        uvc          [[ description = [ UVC Video Camera H.264 Support ] ]]
        vdpau        [[ description = [ Offload parts of video encoding to the GPU using VDPAU ] ]]
        vo-aacenc    [[ description = [ AAC encoder using vo-aacenc ] ]]
        vo-amrwbenc  [[ description = [ OpenCORE AMR-WB decoder and encoder (audio) ] ]]
        wayland      [[ description = [ wayland client video renderer ] ]]
        webp         [[ description = [ Support WebP image decoding using libwebp ] ]]
        xvid         [[ description = [ Xvid encoding and decoding using the xvidcore library ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    gstreamer_plugins:apex? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
    gstreamer_plugins:dtls? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
"

# cogorc_resample_horiz_1tap/2tap - compiled function: compile failed
RESTRICT="test"

# Fails to build currently, so I disabled it below to avoid automagic
#        qt           [[ description = [ Qt5 Qml GL video sink ] ]]
#        gstreamer_plugins:qt? (
#            x11-libs/qtbase:5
#            x11-libs/qtdeclarative:5
#            x11-libs/qtx11extras:5
#        )
DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.31.1] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.12] )
    test:
        media-libs/libexif[>=0.6.16]
    build+run:
        dev-libs/glib:2[>=2.40.0]
        dev-libs/orc:0.4[>=0.4.17]
        media-libs/gstreamer:1.0[>=${PV}]
        media-libs/libpng:=[>=1.0]
        media-plugins/gst-plugins-base:1.0[>=${PV}]
        gstreamer_plugins:apex? (
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl )
        )
        gstreamer_plugins:ass? ( media-libs/libass[>=0.10.2] )
        gstreamer_plugins:bluez? (
            net-wireless/bluez[>=5.0]
            sys-apps/dbus
        )
        gstreamer_plugins:bs2b? ( media-libs/libbs2b[>=3.1.0] )
        gstreamer_plugins:chromaprint? ( media-libs/chromaprint )
        gstreamer_plugins:curl? ( net-misc/curl[>=7.35.0] )
        gstreamer_plugins:dash? ( dev-libs/libxml2:2.0[>=2.8] )
        gstreamer_plugins:dc1394? ( media-libs/libdc1394:2[>=2.0.0] )
        gstreamer_plugins:dtls? (
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl[>=1.0.1] )
        )
        gstreamer_plugins:dts? ( media-libs/libdca )
        gstreamer_plugins:eglgles? ( x11-dri/mesa )
        gstreamer_plugins:faac? ( media-libs/faac )
        gstreamer_plugins:faad? ( media-libs/faad2[>=2.7] )
        gstreamer_plugins:fdk-aac? ( media-libs/fdk-aac )
        gstreamer_plugins:gsm? ( media-libs/gsm )
        gstreamer_plugins:gtk3? ( x11-libs/gtk+:3[>=3.15.0] )
        gstreamer_plugins:hdr? ( media-libs/openexr )
        gstreamer_plugins:hevc? ( media-libs/x265 )
        gstreamer_plugins:jpeg2000? ( media-libs/OpenJPEG:2[>=2.0] )
        gstreamer_plugins:kate? ( media-libs/libkate[>=0.1.7]
                                  media-libs/libtiger[>=0.3.2] )
        gstreamer_plugins:ladspa? ( media-libs/ladspa-sdk
                                    media-libs/liblrdf )
        gstreamer_plugins:libmms? ( media-libs/libmms[>=0.4] )
        gstreamer_plugins:lv2? ( media-libs/lilv[>=0.22] )
        gstreamer_plugins:musepack? ( media-libs/libmpcdec )
        gstreamer_plugins:mjpeg? ( media-video/mjpegtools[>=2.0.0] )
        gstreamer_plugins:neon? ( net-misc/neon[>=0.27.0&<=0.30.99] )
        gstreamer_plugins:ofa? ( media-libs/libofa[>=0.9.3] )
        gstreamer_plugins:openal? ( media-libs/openal[>=1.14] )
        gstreamer_plugins:opencv? ( media-libs/opencv[>=2.3.0&<3.1.0] )
        gstreamer_plugins:opengl? (
            x11-dri/glu
            x11-dri/mesa
            x11-libs/graphene:1.0[>=1.4.0]
        )
        gstreamer_plugins:openh264? ( media-libs/openh264 )
        gstreamer_plugins:resin? ( media-libs/libdvdnav[>=4.1.2]
                                   media-libs/libdvdread[>=4.1.2] )
        gstreamer_plugins:rtmp? ( media-video/rtmpdump )
        gstreamer_plugins:schroedinger? ( media-libs/schroedinger[>=1.0.10] )
        gstreamer_plugins:sdl? ( media-libs/SDL:0 )
        gstreamer_plugins:sndfile? ( media-libs/libsndfile[>=1.0.16] )
        gstreamer_plugins:soundtouch? ( media-libs/soundtouch[>=1.4] )
        gstreamer_plugins:svg? (
            gnome-desktop/librsvg:2[>=2.36.2]
            x11-libs/cairo[>=1.0]
        )
        gstreamer_plugins:uvc? (
            gnome-desktop/libgudev
            media-plugins/gst-plugins-base:1.0 [[ note = [ for gstreamer-video-1.0 ] ]]
            virtual/usb:1
        )
        gstreamer_plugins:vdpau? ( x11-libs/libvdpau )
        gstreamer_plugins:vo-aacenc? ( media-libs/vo-aacenc[>=0.1.0] )
        gstreamer_plugins:vo-amrwbenc? ( media-libs/vo-amrwbenc[>=0.1.0]  )
        gstreamer_plugins:wayland? (
            sys-libs/wayland[>=1.0.0]
            sys-libs/wayland-protocols[>=1.4]
        )
        gstreamer_plugins:webp? ( media-libs/libwebp:=[>=0.2.1] )
        gstreamer_plugins:xvid? ( media-libs/xvid )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        X? ( x11-libs/libX11 )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-experimental'
    '--disable-examples'

    # core plugins
    '--enable-accurip'
    '--enable-adpcmdec'
    '--enable-adpcmenc'
    '--enable-aiff'
    '--enable-asfmux'
    '--enable-audiofxbad'
    '--enable-audiomixer'
    '--enable-audiovisualizers'
    '--enable-autoconvert'
    '--enable-bayer'
    '--enable-camerabin2'
    '--enable-cdxaparse'
    '--enable-coloreffects'
    '--enable-compositor'
    '--enable-dataurisrc'
    '--enable-dccp'
    '--enable-debugutils'
    '--enable-dvbsuboverlay'
    '--enable-dvdspu'
    '--enable-faceoverlay'
    '--enable-festival'
    '--enable-fieldanalysis'
    '--enable-freeverb'
    '--enable-frei0r'
    '--enable-gaudieffects'
    '--enable-gdp'
    '--enable-geometrictransform'
    '--enable-hdvparse'
    '--enable-id3tag'
    '--enable-inter'
    '--enable-interlace'
    '--enable-ivtc'
    '--enable-jpegformat'
    '--enable-librfb'
    '--enable-midi'
    '--enable-mpegdemux'
    '--enable-mpegtsdemux'
    '--enable-mpegtsmux'
    '--enable-mpegpsmux'
    '--enable-mve'
    '--enable-mxf'
    '--enable-nuvdemux'
    '--enable-patchdetect'
    '--enable-orc'
    '--enable-pcapparse'
    '--enable-pnm'
    '--enable-rawparse'
    '--enable-removesilence'
    '--enable-sdi'
    '--enable-sdp'
    '--enable-segmentclip'
    '--enable-shm'
    '--enable-siren'
    '--enable-smooth'
    '--enable-speed'
    '--enable-subenc'
    '--enable-tta'
    '--enable-videofilters'
    '--enable-videomeasure'
    '--enable-videoparsers'
    '--enable-y4m'
    '--enable-yadif'

    # moved to gst-plugins-base
    '--disable-opus'

    # Windows Specific
    '--disable-acm'
    '--disable-direct3d'
    '--disable-directsound'
    '--disable-wasapi'
    '--disable-wininet'
    '--disable-winscreencap'

    # OS X Specific
    '--disable-apple-media'
    '--disable-avc'

    # OpenBSD Specific
    '--disable-sndio'

    # Unpackaged dependencies
    '--disable-daala'
    '--disable-directfb'
    '--disable-flite'
    '--disable-gme'
    '--disable-libde265'
    '--disable-mimic'
    '--disable-modplug'
    '--disable-nas'
    '--disable-pvr'
    '--disable-spandsp'
    '--disable-spc'
    '--disable-timidity'
    '--disable-wildmidi'
    '--disable-zbar'

    '--disable-fluidsynth'
    '--disable-hls'
    '--disable-kms'
    '--disable-sbc'
    '--disable-smoothstreaming'
    '--disable-srtp'
    '--disable-vulkan'
    '--disable-webrtcdsp'

    # Fails to build currently
    '--disable-qt'

    '--enable-bz2'
    '--enable-decklink'
    '--enable-dvb'
    '--enable-fbdev'
    '--enable-linsys'
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'debug'
    'gobject-introspection introspection'
    'gtk-doc'
    'X x11'

    'gstreamer_plugins:apex apexsink'
    'gstreamer_plugins:ass assrender'
    'gstreamer_plugins:bs2b'
    'gstreamer_plugins:chromaprint'
    'gstreamer_plugins:curl'
    'gstreamer_plugins:dash'
    'gstreamer_plugins:dc1394'
    'gstreamer_plugins:dtls'
    'gstreamer_plugins:dts'
    'gstreamer_plugins:eglgles egl'
    'gstreamer_plugins:eglgles gles2'
    'gstreamer_plugins:faac'
    'gstreamer_plugins:faad'
    'gstreamer_plugins:fdk-aac fdk_aac'
    'gstreamer_plugins:gsm'
    'gstreamer_plugins:gtk3'
    'gstreamer_plugins:hdr openexr'
    'gstreamer_plugins:hevc x265'
    'gstreamer_plugins:jpeg2000 openjpeg'
    'gstreamer_plugins:kate'
    'gstreamer_plugins:ladspa'
    'gstreamer_plugins:libmms'
    'gstreamer_plugins:lv2'
    'gstreamer_plugins:musepack'
    'gstreamer_plugins:mjpeg mpeg2enc'
    'gstreamer_plugins:mjpeg mplex'
    'gstreamer_plugins:neon'
    'gstreamer_plugins:ofa'
    'gstreamer_plugins:openal'
    'gstreamer_plugins:opencv'
    'gstreamer_plugins:opengl'
    'gstreamer_plugins:opengl glx'
    'gstreamer_plugins:openh264'
    'gstreamer_plugins:resin resindvd'
    'gstreamer_plugins:rtmp'
    'gstreamer_plugins:schroedinger schro'
    'gstreamer_plugins:sdl'
    'gstreamer_plugins:sndfile'
    'gstreamer_plugins:soundtouch'
    'gstreamer_plugins:svg rsvg'
    'gstreamer_plugins:uvc uvch264'
    'gstreamer_plugins:vdpau'
    'gstreamer_plugins:vo-aacenc voaacenc'
    'gstreamer_plugins:vo-amrwbenc voamrwbenc'
    'gstreamer_plugins:wayland'
    'gstreamer_plugins:webp'
    'gstreamer_plugins:xvid'
)

src_test() {
    unset DBUS_SESSION_BUS_ADDRESS
    unset DISPLAY
    default
}


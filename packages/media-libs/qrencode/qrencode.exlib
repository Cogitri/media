# Copyright 2013-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 ] ]
require alternatives

export_exlib_phases src_compile src_test src_install

SUMMARY="C library for encoding data in a QR code symbol"
DESCRIPTION="QR codes are a kind of 2D symbology that can be scanned by handy terminals such as a
mobile phone with CCD. The capacity of QR Code is up to 7000 digits or 4000 characters."
HOMEPAGE="http://fukuchi.org/works/${PN}"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.bz2"

LICENCES="LGPL-2.1"
MYOPTIONS="
    doc
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        doc? ( app-doc/doxygen )
    build+run:
        media-libs/libpng:= [[ description = [ needed for tools and tests ] ]]
    run:
        !media-libs/qrencode:0[<3.4.4-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

if ever at_least 4.0.0 ; then
    DEPENDENCIES+="
        test:
            media-libs/SDL:2[>=2.0.0]
    "
else
    DEPENDENCIES+="
        test:
            media-libs/SDL:0[>=1.2.0]
    "
fi

BUGS_TO="heirecka@exherbo.org"

DEFAULT_SRC_CONFIGURE_TESTS=( --with-tests )

qrencode_src_compile() {
    default

    option doc && edo doxygen "${WORK}"/Doxyfile
}

qrencode_src_test() {
    edo pushd tests
    edo sh "${WORK}"/tests/test_all.sh
    edo popd
}

qrencode_src_install() {
    local alternatives=()
    local host=$(exhost --target)

    default

    option doc && dodoc -r html

    alternatives+=(
        /usr/share/man/man1/${PN}.1            ${PN}-${SLOT}.1
        /usr/${host}/bin/${PN}                 ${PN}-${SLOT}
        /usr/${host}/include/${PN}.h           ${PN}-${SLOT}.h
        /usr/${host}/lib/lib${PN}.la           lib${PN}-${SLOT}.la
        /usr/${host}/lib/lib${PN}.so           lib${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/lib${PN}.pc lib${PN}-${SLOT}.pc
    )

    alternatives_for _${PN} ${SLOT} ${SLOT} "${alternatives[@]}"

}


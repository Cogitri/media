# Copyright 2010 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever at_least 1.4.0; then
    MY_PNV=${PNV}
else
    MY_PNV=${PN}-v${PV}
fi

require googlecode [ project=webm ] flag-o-matic

export_exlib_phases pkg_setup src_prepare src_configure

SUMMARY="VP8/VP9 Codec SDK"
HOMEPAGE="https://www.webmproject.org"
DOWNLOADS="http://downloads.webmproject.org/releases/webm/${MY_PNV}.tar.bz2"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    examples
    platform: amd64 x86
"

# requires internet access
RESTRICT="test"

DEPENDENCIES="
    build:
        platform:amd64? ( dev-lang/yasm )
        platform:x86? ( dev-lang/yasm )
"

LIBVPX_MAKE_PARAMS=(
    verbose=true
    HAVE_GNU_STRIP=no
)

DEFAULT_SRC_COMPILE_PARAMS=( ${LIBVPX_MAKE_PARAMS[@]} )
DEFAULT_SRC_INSTALL_PARAMS=( ${LIBVPX_MAKE_PARAMS[*]} )
DEFAULT_SRC_INSTALL_EXCLUDE=( release.sh )

WORK=${WORKBASE}/${MY_PNV}

libvpx_pkg_setup() {
    export AS=$(_libvpx_assembler)

    # use prefixed gcc for linking
    export LD=$(exhost --tool-prefix)gcc

    filter-flags -ggdb3
}

libvpx_src_prepare() {
    default

    # Custom configure script. Unconditionally enables building of documentation when doxygen and
    # php are found. We just pretend doxygen does not exist
    edo sed -e '/^doxy_version=/d' -i configure
}

_libvpx_assembler() {
    case "$(exhost --target)" in
    i686-*|x86_64-*)
        echo yasm
    ;;
    arm*)
        echo $(exhost --tool-prefix)as
    ;;
    esac
}

_libvpx_target() {
    case "$(exhost --target)" in
    aarch64-*)
        echo arm64-linux-gcc
    ;;
    armv7-*)
        echo armv7-linux-gcc
    ;;
    i686-*)
        echo x86-linux-gcc
    ;;
    x86_64-*)
        echo x86_64-linux-gcc
    ;;
    *)
        die "Unknown libvpx target for $(exhost --target)"
    ;;
    esac
}

libvpx_src_configure() {
    local myconf=(
        --prefix=/usr/$(exhost --target)
        --target=$(_libvpx_target)
        --enable-pic
        --enable-runtime-cpu-detect
        --enable-shared
        --enable-vp{8,9}
        --enable-{,vp9-}postproc
        --disable-optimizations # Otherwise CFLAGS are overridden
        --disable-static
        $(expecting_tests && echo --enable-unit-tests || echo --disable-unit-tests)
        $(option_enable examples)
    )

    # experimental and spatial-svc are required for chromium
    if ever at_least 1.6.1; then
        myconf+=(
            --enable-experimental
            --enable-spatial-svc
            --enable-tools
        )
    fi

    edo ./configure ${myconf[*]}
}


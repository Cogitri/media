# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN="${PN/-/_}"

SUMMARY="Linux Audio Developer's Simple Plugin API"
DESCRIPTION="
There is a large number of synthesis packages in use or development
on the Linux platform at this time. This API ('The Linux Audio
Developer's Simple Plugin API') attempts to give programmers the
ability to write simple 'plugin' audio processors in C/C++ and link
them dynamically ('plug') into a range of these packages ('hosts').
It should be possible for any host and any plugin to communicate
completely through this interface.
"
HOMEPAGE="https://en.wikipedia.org/wiki/LADSPA"
DOWNLOADS="http://http.debian.net/debian/pool/main/l/${PN}/${PN}_${PV}.orig.tar.gz"

BUGS_TO="alip@exherbo.org"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# Tests try to play audio
RESTRICT="test"

DEPENDENCIES=""

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PNV}-makefile-default.patch )

WORK="${WORKBASE}/${MY_PN}/src"

src_prepare() {
    default

    # Respect DESTDIR
    edo sed \
        -e "s:/usr/lib/ladspa:\$(DESTDIR)/usr/$(exhost --target)/lib/ladspa:" \
        -e "s:/usr/include:\$(DESTDIR)/usr/$(exhost --target)/include:" \
        -e "s:/usr/bin:\$(DESTDIR)/usr/$(exhost --target)/bin:" \
        -i makefile

    # Use mkdir -p
    edo sed \
        -e "s:mkdirhier:mkdir -p:" \
        -i makefile

    # Respect CC, CXX and CFLAGS
    edo sed \
        -e "s:cc:${CC}:" \
        -e "s:c++:${CXX}:" \
        -e "s:-O3:${CFLAGS}:" \
        -i makefile
}

src_install() {
    default

    hereenvd 80ladspa <<EOF
LADSPA_PATH="/usr/$(exhost --target)/lib/ladspa"
EOF
    dodoc ../README
}


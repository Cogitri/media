# Copyright 2014-2015 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=mltframework tag=v${PV} ] \
        qmake [ slot=5 ] \
        freedesktop-desktop \
        gtk-icon-cache

SUMMARY="Shotcut is a free, open source, cross-platform video editor"
HOMEPAGE+=" http://www.shotcut.org"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-libs/qttools:5 [[ note = [ lrelease/lconvert for translations ] ]]
    build+run:
        media/mlt[>=0.9.4][ffmpeg][frei0r][qt5]
        x11-dri/mesa [[ note = [ provides libGL ] ]]
        x11-libs/libX11
        x11-libs/qtmultimedia:5
        x11-libs/qtx11extras:5
    run:
        media/ffmpeg[>=2.3][X][h264][hevc][mp3][opus][pulseaudio][theora][vp8]
        media-libs/SDL:0[>=1.2]
        media-sound/jack-audio-connection-kit
"

EQMAKE_PARAMS+=( PREFIX=/usr )

src_prepare() {
    default

    edo lrelease-qt5 translations/${PN}_*.ts
}

src_install() {
    default

    insinto /usr/share/${PN}
    doins -r src/qml

    for x in 64; do
        insinto /usr/share/icons/hicolor/${x}x${x}/apps
        newins icons/${PN}-logo-${x}.png ${PN}.png
    done

    insinto /usr/share/${PN}/translations
    doins translations/*.qm

    insinto /usr/share/applications
    hereins ${PN}.desktop <<EOF
[Desktop Entry]
Name=Shotcut
Name[de]=Shotcut
GenericName=Video Editor
GenericName[de]=Video Bearbeitungsprogramm
Comment=Video Editor
Comment[de]=Programm zum Bearbeiten und Abspielen von Videodateien.
Type=Application
Exec=shotcut "%F"
Icon=shotcut
Terminal=false
Categories=Qt;KDE;AudioVideo;AudioVideoEditing;
EOF

    emagicdocs

    edo rm -r "${IMAGE}"/var
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}


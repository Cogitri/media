# Copyright 2016 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV=${PV/_/-}

require github [ user=vkohaupt ] qmake [ slot=5 ] freedesktop-desktop

SUMMARY="Easy to use screencast creator"
DESCRIPTION="
vokoscreen is an easy to use screencast creator to record educational videos, live recordings of
browser, installation, videoconferences, etc.
"
HOMEPAGE+=" http://linuxecke.volkoh.de/${PN}/${PN}.html"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        x11-libs/libXrandr [[ note = [ X11/extensions/Xrandr.h ] ]]
        x11-libs/qttools:5 [[ note = [ lrelease/lconvert for translations ] ]]
        x11-proto/xproto
    build+run:
        sys-sound/alsa-lib
        media-libs/v4l-utils
        x11-dri/mesa [[ note = [ provides libGL ] ]]
        x11-libs/libX11
        x11-libs/qtbase:5[gui]
        x11-libs/qtx11extras:5
    run:
        media-sound/pulseaudio
        sys-process/lsof [[ note = [ check if sound and v4l devices are busy ] ]]
        x11-apps/xdg-utils [[ note = [ send video button ] ]]
        providers:ffmpeg? ( media/ffmpeg[>=1.1.0][h264][vorbis] )
        providers:libav? ( media/libav[h264][vorbis] )
"

EQMAKE_PARAMS=(
    DEFINES+=NO_NEW_VERSION_CHECK
    PREFIX=/usr/$(exhost --target)
)

src_prepare() {
    default

    # https://github.com/vkohaupt/vokoscreen/issues/144
    edo sed \
        -e 's:$$PREFIX/share:/usr/share:g' \
        -i ${PN}.pro
}

